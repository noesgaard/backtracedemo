# backtracedemo

https://github.com/ianlancetaylor/libbacktrace

this library is BSD license so it can be linked with software that is not released to the public.

# Note
Run the demos in a terminal, not through SSH, otherwise it will not generate a core dump file.
