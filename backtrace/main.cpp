#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <backtrace.h>

static void error_callback (void *data, const char *message, int error_number) {
  if (error_number == -1) {
    fprintf(stderr, "If you want backtraces, you have to compile with -g\n\n");
    _exit(1);
  } else {
    fprintf(stderr, "Backtrace error %d: %s\n", error_number, message);
  };
};

static int full_callback (void *data, uintptr_t pc, const char *pathname, int line_number, const char *function) {
  static int unknown_count = 0;
  if (pathname != NULL || function != NULL || line_number != 0) {
    if (unknown_count) {
      fprintf(stderr, "    ...\n");
      unknown_count = 0;
    };
    const char *filename = rindex(pathname, '/');
    if (filename) filename++; else filename = pathname;
    fprintf(stderr, "  %s:%d -- %s\n", filename, line_number, function);
  } else {
    unknown_count++;
  };
  return 0;
};

struct backtrace_state *state;

void sigsegv_handler (int number) {
  fprintf(stderr, "\n*** Segmentation Fault ***\n\n");
  backtrace_full(state, 0, full_callback, error_callback, NULL);
  printf("\n");
  backtrace_print(state, 0, stderr);
  _exit(1);
};

void function_two () {
  *((void **) 0) = 0; // program crashes here
};

void function_one () {
  function_two();
};

int main () {
  signal(SIGSEGV, sigsegv_handler);
  state = backtrace_create_state(NULL, 0, error_callback, NULL);
  function_one();
  return 0;
};
