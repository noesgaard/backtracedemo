
#include <signal.h>
#include <stdio.h>
#include <string.h>

#include <iostream>
#include <stdexcept>

using std::cout;
using std::endl;

void signalHandler(int signum)
{
  throw(std::range_error("Floating point exception"));
}


void function_two () {
  int a = 1.0;
  int b = 0.0;
  int c = a/b;
  cout << "Value " << c << endl;
};

void function_one () {
  function_two();
};

int main () {
  signal(SIGFPE, signalHandler);
  try {
    function_one();
  }
  catch (std::exception &ex) {
    cout << "Exception: " << ex.what() << endl;
  }
  return 0;
}
