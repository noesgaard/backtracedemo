gcc extra.cpp -g -o extra

objcopy extra extra.dbg

objcopy --only-keep-debug extra extra.dbg

strip --strip-debug --strip-unneeded extra

objcopy --add-gnu-debuglink=extra.dbg extra
