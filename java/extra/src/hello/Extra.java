package hello;

public class Extra {

	public static void main(String[] args) {
		CoolStuff cool = new CoolStuff();
		try {
			System.out.println(cool.f2());
		}
		catch (Exception ex) {
			System.out.println("Exception message");
			System.out.println(ex.getMessage());
			System.out.println("Exception backtrace");
			ex.printStackTrace(System.out);
		}
	}

}
