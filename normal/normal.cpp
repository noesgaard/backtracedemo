#include <stdio.h>
#include <string.h>

void function_two () {
  *((void **) 0) = 0; // program crashes here
};

void function_one () {
  function_two();
};

int main () {
  function_one();
  return 0;
};
